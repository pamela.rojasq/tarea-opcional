## Tarea "Gobernar el Nuevo Mundo de los Datos"  
### Nombre: Pamela Rojas
### Rol: 201973060-6
### 1. Resumen
### 2. Opinión
<br>
 <br>
1. <br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; El artículo se centra en exponer la problemática administración de los datos en la sociedad actual.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Comienza presentando el contexto de los datos, enfatizando como el avance tecnológico a ayudado en gran medida en la recolección de datos, y además en exponer la tesis del texto, que consiste en aclararnos la influencia que tienen los datos en los ámbitos culturales, educativos, organizacionales, etc. Y como cada individuo debería tener la capacidad de decidir que se hace con sus datos.<br>  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Posteriormente, explica como la disciplina de bases de datos almacena, cuida, mantiene y retorna (cuando son consultados) datos a través de softwares y algoritmos. Donde los Sistemas de Información se encargan de estructurar y organizar los recursos (datos), y la Gestión de Datos se encarga de planificar los procesos para que los recursos circulen de forma fluida. Sin embargo, con el creciente número de datos, el autor comenzó a cuestionar sobre cuales son los dueños de esos recursos, de que manera los están usando, como los están recolectando, etc. Para proseguir con un ejemplo, las autoridades chilenas privatizando los datos relacionados con la pandemia mundial vigente y manipulándolos, puesto que nadie tenía acceso a ellos y nadie los monitoreaba.<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Luego, detalla la diferencia entre los términos administración y gobierno, y como es importante que comencemos a gobernar estos datos cuando hoy en día están siendo usados para dirigir las acciones de las personas. A continuación se enfoco en 5 asuntos para que los lectores comiencen a cuestionarse está problemática:<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Datos Personales.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; La responsabilidad de gobernar sobre el flujo de recepción y envío de datos, debido a que las nuevas tecnologías han permitido que el ciudadano común reciba estos recursos de forma directa y a su vez, se vuelva un productor de ellos.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Datos en el Espacio Público.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; La interacción a larga distancia dio a lugar que los datos entregados en ello puedan ser usados en estadísticas, provocando que se vea afectado el derecho de privacidad y otros derechos más.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Gobierno de Datos en Empresas y Organizaciones.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Donde se emplea la palabra "gobernanza" sobre "gobierno". Explica sobre como la gobernanza de datos toma decisiones sobre las personas responsables de la calidad de los datos, gestión, políticas y procesos, entre otros más. Por lo que se pueden reconocer 5 dominios de la Tecnología de Información (TI):<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Principios.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Arquitectura.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Infraestructura.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Necesidades de Aplicaciones Comerciales.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Inversión y Priorización.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Y con esto, 5 dominios interrelacionados en la toma de decisiones:<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Principios de datos.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Calidad de datos.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Metadatos.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Acceso de datos.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -Ciclo tradicional de datos.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4. Gobierno de Datos a Nivel País.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nombra objetivos a tener en cuenta para proteger la soberanía del país y mantener la identidad nacional, los cuales son preservar una sociedad abierta y la democracia, realzar y respetar la privacidad, entre otros.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5. Gobierno de Datos internacionales.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Explica como los datos pueden sortear fronteras materiales y destructurar el tiempo tradicional, lo cual codician las empresas y lo que se le dificulta a los Estados, por lo que los Estados y compañías entran en conflicto.<br>   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Por último, entrega una conclusión donde llama a reflexionar sobre el tema, puesto que las responsabilidad sobre los datos van en aumento con el constante crecimiento de los datos, por lo que es muy relevante que aprendamos a gobernar sobre ellos.<br>   
 <br>
2.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; El texto fue muy interesante de leer, porque toca un tema realmente relevante en la actualidad. Cuando son nuestros datos los que están siendo usados, pero no sabemos quien termina siendo dueño de estos y como los utilizara, si será a nuestro beneficio o para el beneficio personal de unos pocos, es preocupante y asusta. Por lo que me parece demasiado relevante que se creen leyes que limiten sobre los datos, que regularicen y protejan, para que no pueda ser usados contra la gente común y corriente. 
